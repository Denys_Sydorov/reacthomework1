import React from 'react';
import ContentHeader from '../contentHeader';
import ContentText from '../contentText';
import ContentButton from '../contentButton';

import './content.css';

const Content = () => {                       // Content делим не 3 модуля.
    return (
        <div className='div_content'>
            <ContentHeader></ContentHeader>  {/* ContentHeader - заглавие материала */}
            <ContentText></ContentText>      {/* ContentText - материал */}
            <ContentButton></ContentButton>  {/* ContentButton - кнопка Get Started */}
        </div>
    )
}

export default Content;