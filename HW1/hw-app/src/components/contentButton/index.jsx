import React from 'react';

import './contentButton.css'

const ContentButton = () => {
    return (
        <div className='div_button'>
            <button className='content_button'>get started</button>
        </div>            
    )
}

export default ContentButton;