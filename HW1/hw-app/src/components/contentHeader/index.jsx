import React from 'react';

import './contentHeader.css';

const ContentHeader = () => {                                   // Заглавие Контента, шапка текстового модуля
    return (
        <div className='div_content_header'>
            Rappresent your life with a simple photo
        </div>
    )
}

export default ContentHeader;