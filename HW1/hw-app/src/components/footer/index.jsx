import React from 'react';

import './footer.css';

const Footer = () => {                                      // обычный Footer
    return (
        <div className='div_footer'>
            Copyright by phototime - all right reserved
        </div>

    )
}

export default Footer;