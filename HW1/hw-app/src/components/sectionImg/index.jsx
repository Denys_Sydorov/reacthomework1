import React from 'react';
import img from './topImg.png';                                            // Импортируем изображение в модуль и присваиваем имя

import './sectionImg.css'                                           

const SectionImg = () => {
    return <img className='section_img' src={img} alt='img'></img>         // Теперь мы можем вывести изображение на страницу
}

export default SectionImg;