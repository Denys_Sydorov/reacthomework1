import React from 'react';

import './header.css'

const Header = ({headerArr}) => {                                                        // из модуля "Main" подтягиваем "props = headerArr" 
    return (
        <div className='header'>
            <ul>
                {headerArr.map( (header, index) => {                                     // используя метод "map" перебираем массив "headerArr" и выводим в шапку сайта "Header" полученные данные
                    return (
                            <li key={index*4 + 'b'}><a href='../../'>{header}</a></li>  
                    )
                })}
            </ul>
        </div>
    )
}

export default Header;