import React from 'react';
import Header from '../header';
import SectionImg from '../sectionImg';
import Content from '../content';
import Footer from '../footer';

import './main.css'

const Main = () => {
    const headerArr = ['Home', 'Photoapp', 'Design', 'Download'];      // Создаем массив для модуля "Header", для меню навигации

    return (
        <div className='div_main'>
            <Header headerArr={headerArr}></Header>                    {/* Меню навигации в шапке сайта*/}
            <SectionImg></SectionImg>                                  {/* Изображение на странице */}
            <Content></Content>                                        {/* Контент который состоит из 3 модулей собраных в один модуль "Content" */}
            <Footer></Footer>                                          {/* Подвал сайта, footer и в Африке footer*/}
        </div>
    )
}

export default Main;